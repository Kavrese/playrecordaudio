package com.example.playrecordaudio.model

class ModelAudio (
    var name: String? = null,
    var date: String? = null,
    var path: String? = null
)